pub mod filewatcher;
mod http;
mod processor;

use std::{net::TcpListener, path, path::PathBuf, sync::Arc, thread, string, vec};

use clap::Parser;
use filewatcher::{FileWatcher, Update};
use http::handler::HttpHandler;
use processor::{sass::SassProcessor, Processor};

pub use filewatcher::AssetType;

const HTTP_ADDRESS: &str = "127.0.0.1:7878";

/// CLI Options
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
   /// Sets the path to watch for updates
   #[arg(short, long)]
   path: Option<String>,

   /// File extensions to ignore
   #[arg(short, long)]
   ignore: Option<Vec<String>>,
}

fn main() {
    let mut watch_path = PathBuf::from(".");
    let mut ignored_extensions = Vec::new();

    let args = Args::parse();

    if let Some(config_path) = args.path.as_deref() {
        watch_path = path::PathBuf::from(config_path);
    }

    if let Some(extensions) = args.ignore.as_deref() {
        ignored_extensions = extensions.to_vec();

        println!("Ingnoring file extensions: {:?}", ignored_extensions);
    }

    // Our channel to receive updates from the file watcher thread
    let (updates_tx, updates_rx) = crossbeam::channel::unbounded();

    let web_root = Arc::new(watch_path.clone());

    let file_watcher = FileWatcher::new(web_root.clone().to_path_buf(), ignored_extensions.to_vec());

    // Watch for assets changes in a new thread and notify the websocket update channel
    thread::spawn(move || {
        while let Ok(event) = file_watcher.asset_updates.recv() {
            println!("WATCHER_CHANNEL RCV: \t\t{:?}", event.path);

            // If an asset is processed transformed from '.scss' to '.css' the file watcher
            // will emit its own event for the file written by the processor, so we don't need to send it
            if let Some(update) = process_asset(event.clone(), web_root.to_path_buf()) {
                updates_tx.send(update.to_string()).unwrap();
            }
        }
    });

    // Each new http connection may want to be notified of these events
    let updates = Arc::new(updates_rx);

    // Rig up the HTTP handler, push new connections into a thread
    let http_listener = TcpListener::bind(HTTP_ADDRESS).unwrap();

    println!("HTTP listening on http://{}", HTTP_ADDRESS);

    for stream in http_listener.incoming() {
        let updates_local = Arc::clone(&updates);
        let web_root = watch_path.clone();

        thread::spawn(move || {
            HttpHandler::handle(stream.unwrap(), updates_local, web_root.to_path_buf());
        });
    }
}

fn process_asset(update: Update, web_root: path::PathBuf) -> Option<Update> {
    match update.asset_type {
        AssetType::SCSS => SassProcessor::process(update, web_root),
        _ => Some(update),
    }
}

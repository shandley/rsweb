use crossbeam::{channel, Receiver};
use notify::{event::ModifyKind, EventKind, RecommendedWatcher, RecursiveMode, Watcher};
use std::{fmt, path::PathBuf};

#[derive(PartialEq, Clone)]
pub enum AssetType {
    CSS,
    SCSS,
    Markdown,
    JS,
    MJS,
    HTML,
    Unknown,
}

#[derive(Clone)]
pub struct Update {
    pub asset_type: AssetType,
    pub path: Option<String>,
}

impl From<(&PathBuf, &PathBuf)> for Update {
    fn from(data: (&PathBuf, &PathBuf)) -> Self {
        let (request_path, web_root) = data;

        Self {
            asset_type: AssetType::from(request_path),
            path: Self::path_relative_to_web_root(request_path, web_root),
        }
    }
}

impl Update {
    // in: ../blah/webroot/header.js
    // out: header.js
    pub fn path_relative_to_web_root(path: &PathBuf, web_root: &PathBuf) -> Option<String> {
        // return from right: parts until 'src'
        if let Ok(stripped_path) = path.strip_prefix(web_root) {
            return stripped_path.to_str().map(String::from);
        } else {
            println!(
                "Failed to strip prefix from path: {:?}",
                path.to_str().map(String::from)
            );
        }

        None
    }
}

impl From<&PathBuf> for AssetType {
    fn from(path: &PathBuf) -> Self {
        match path.extension() {
            Some(extension) => match extension.to_str() {
                Some("css") => Self::CSS,
                Some("scss") => Self::SCSS,
                Some("js") => Self::JS,
                Some("mjs") => Self::MJS,
                Some("md") => Self::Markdown,
                Some("html") => Self::HTML,
                _ => Self::Unknown,
            },
            _ => Self::Unknown,
        }
    }
}

impl fmt::Display for Update {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "event: {}\r\ndata: {}\r\n\r\n",
            match &self.asset_type {
                AssetType::CSS => "css",
                AssetType::SCSS => "scss",
                AssetType::JS | AssetType::MJS => "js",
                AssetType::HTML => "html",
                _ => "unknown",
            },
            &self.path.clone().unwrap_or_default()
        )
    }
}

pub struct FileWatcher {
    pub asset_updates: Receiver<Update>,
    _watcher: notify::fsevent::FsEventWatcher,
    _watch_path: PathBuf,
}

impl FileWatcher {
    pub fn new(path: PathBuf, ignored_extensions: Vec<String>) -> Self {
        // Ensure our watch path is legit
        let watch_path = match path.canonicalize() {
            Ok(path) => {
                println!("File watcher is watching {:?}", path);
                path
            }
            Err(error) => panic!("Path error: {:?}", error),
        };

        let (asset_updates_notify, asset_updates) = channel::unbounded();

        let mut _watcher: RecommendedWatcher = notify::recommended_watcher(
            move |res: Result<notify::event::Event, notify::Error>| match res {
                Ok(update) => {
                    // Filter out metadata changes, so we only get the content change event
                    if update.kind.is_modify() {
                        match update.kind {
                            EventKind::Modify(ModifyKind::Data(_)) => {
                                if let Some(path) = update.paths.first() {
                                    // TODO, SASS builds to css, updates file, then file watcher says, hey CSS update, and it fetches again :o
                                    let msg = Update::from((path, &watch_path.to_path_buf()));

                                    // Ignore internal paths
                                    if path.is_dir() || path.starts_with("wasm/target") || path.ends_with(".gitignore") {
                                        println!("Ignoring path: {:?}", &path);

                                        return {}
                                    }

                                    // Check that the file extension is not ignored
                                    if let Some(extension) = path.extension().and_then(|f| f.to_str()) {
                                        if ignored_extensions.contains(&String::from(extension)) {
                                            println!("Ignoring update: \t\tPath: {:?}, due to ignore extension {:?}", &msg.path, extension);
                                        }
                                    } else {
                                        println!("FILE_WATCHER SND: \t\tPath: {:?}", &msg.path);

                                        asset_updates_notify.send(msg).unwrap();
                                    }
                                }
                            }
                            _ => {}
                        }
                    }
                }
                Err(error) => {
                    println!("Received an error event from the file watcher: {:?}", error)
                }
            },
        )
        .unwrap();

        _watcher.watch(&path.as_path(), RecursiveMode::Recursive).unwrap();

        Self {
            asset_updates,
            _watcher,
            _watch_path: path.clone(),
        }
    }
}

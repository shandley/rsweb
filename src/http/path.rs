/// Various (limited) methods for determining the mime type of a file
use std::ffi::OsStr;
use std::fmt;
use std::path::Path;

pub enum MimeTextType {
    Html,
    Css,
}

pub enum MimeImageType {
    Icon,
}

pub enum MimeApplicationType {
    Wasm,
}

/// The list of currently handled mime types is quite small,
/// but given the purpose of this, it doesn't need to be exhaustive
pub enum MimeType {
    Text(MimeTextType),
    Image(MimeImageType),
    Application(MimeApplicationType),
    Javascript,
    Unknown,
}

impl From<&String> for MimeType {
    fn from(path: &String) -> Self {
        if let Some(extension) = Path::new(path).extension().and_then(OsStr::to_str) {
            return match extension {
                "html" => MimeType::Text(MimeTextType::Html),
                "css" => MimeType::Text(MimeTextType::Css),
                "js" | "mjs" => MimeType::Javascript,
                "wasm" => MimeType::Application(MimeApplicationType::Wasm),
                "ico" => MimeType::Image(MimeImageType::Icon),
                _ => MimeType::Unknown,
            };
        }

        MimeType::Unknown
    }
}

impl fmt::Display for MimeType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match &self {
                Self::Text(texttype) => match texttype {
                    MimeTextType::Html => "text/html",
                    MimeTextType::Css => "text/css",
                },
                Self::Image(image_type) => match image_type {
                    MimeImageType::Icon => "image/x-icon",
                },
                Self::Javascript => "application/javascript",
                Self::Application(application_type) => match application_type {
                    MimeApplicationType::Wasm => "application/wasm",
                },
                _ => "text/plain; charset=utf-8",
            }
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_mime_type_html() {
        let path = String::from("../web/src/index.html");
        let mime_type = MimeType::from(&path);

        assert_eq!(mime_type.to_string(), "text/html")
    }

    #[test]
    fn get_mime_type_mjs() {
        let path = String::from("../web/blah/module.mjs");
        let mime_type = MimeType::from(&path);

        assert_eq!(mime_type.to_string(), "application/javascript")
    }

    #[test]
    fn get_mime_type_image() {
        let path = String::from("public/favicon.ico");
        let mime_type = MimeType::from(&path);

        assert_eq!(mime_type.to_string(), "image/x-icon")
    }

    #[test]
    fn get_mime_type_wasm() {
        let path = String::from("public/wasm/module.wasm");
        let mime_type = MimeType::from(&path);

        assert_eq!(mime_type.to_string(), "application/wasm")
    }
}

use std::path::Path;
use std::sync::Arc;
use std::{fs, io::prelude::*, io::ErrorKind, net::TcpStream};

use super::mime::MimeType;

const EVENT_PATH: &str = "/events";

#[derive(Debug, PartialEq)]
enum RequestMethod {
    GET,
    UNHANDLED,
}

impl From<&[u8]> for RequestMethod {
    fn from(data: &[u8]) -> Self {
        let mut iter = data.iter().take(2);

        match iter.next() {
            Some(71) => Self::GET,
            _ => Self::UNHANDLED,
        }
    }
}

struct Request {
    pub method: RequestMethod,
    pub path: String,
}

impl Request {
    pub fn is_event_stream_connection(&self) -> bool {
        &self.path == EVENT_PATH
    }
}

struct Response;

impl Response {
    pub fn not_found() -> String {
        String::from("HTTP/1.1 404 NOT FOUND\r\n\r\n")
    }

    pub fn method_not_allowed() -> String {
        String::from("HTTP/1.1 405 METHOD NOT ALLOWED\r\n\r\n")
    }

    pub fn internal_error() -> String {
        String::from("HTTP/1.1 500 INTERNAL SERVER ERROR\r\n\r\n")
    }
}

pub struct HttpHandler;

impl HttpHandler {
    pub fn handle(
        mut stream: TcpStream,
        events: Arc<crossbeam::Receiver<String>>,
        web_root: std::path::PathBuf,
    ) {
        let mut buffer = [0; 1024];
        stream.read(&mut buffer).unwrap();

        if let Some(request) = Self::parse_request(&buffer, web_root.to_str().unwrap_or_default()) {
            if request.is_event_stream_connection() {
                stream.set_nonblocking(true).unwrap();

                let response = b"\
                    HTTP/1.1 200 OK\r\n\
                    Access-Control-Allow-Origin: *\r\n\
                    Cache-Control: no-cache\r\n\
                    Connection: keep-alive\r\n\
                    Content-Type: text/event-stream\r\n\
                    \r\n";

                stream.write(response).unwrap();

                // say hello to our new friend
                let _ = stream.write("event: hello\r\n\r\n".as_bytes());

                loop {
                    if let Ok(asset_update) = events.recv() {
                        println!("EMITTING UPDATE:\t\t{:?}", &asset_update);
                        
                        match stream.read(&mut buffer) {
                            Ok(0) => {
                                println!("Stream read failed: Connection is closed");
                                return;
                            }
                            Err(error) => {
                                if error.kind() != ErrorKind::WouldBlock {
                                    println!("Stream read failed: ErrorKind::WouldBlock");
                                    return;
                                }
                            }
                            Ok(_) => {}
                        }

                        let _ = stream.write(asset_update.as_bytes());

                        println!("--------------------------------------");
                    }
                }
            } else {
                match fs::read_to_string(&request.path) {
                    Ok(contents) => {
                        let response = format!(
                            "HTTP/1.1 200 OK\r\nContent-Length: {}\r\n{}\r\n{}{}\r\n\r\n{}",
                            contents.len(),
                            "Cache-Control: no-store, must-revalidate\r\nExpires: 0",
                            "Content-type: ",
                            MimeType::from(&request.path),
                            contents
                        );

                        stream.write(response.as_bytes()).unwrap();
                    }
                    Err(error) => {
                        // It migt be some binary file
                        if let Ok(contents) = fs::read(&request.path) {
                            let response = format!(
                                "HTTP/1.1 200 OK\r\nContent-Length: {}\r\n{}{}\r\n\r\n",
                                contents.len(),
                                "Content-type: ",
                                MimeType::from(&request.path),
                            );

                            stream.write(response.as_bytes()).unwrap();
                            if let Err(error) = stream.write(contents.as_slice()) {
                                println!(
                                    "Failed to send binary data: {:?} ({:?})",
                                    error, request.path
                                );
                            }
                        } else {
                            // 404 for missing file, 500 for everything else
                            match error.kind() {
                                std::io::ErrorKind::NotFound => {
                                    stream.write(Response::not_found().as_bytes()).unwrap();
                                }
                                _ => {
                                    println!("File Load Error: {:?} ({:?})", error, request.path);
                                    stream.write(Response::internal_error().as_bytes()).unwrap();
                                }
                            };
                        }
                    }
                }

                stream.flush().unwrap();
            }
        } else {
            stream
                .write(Response::method_not_allowed().as_bytes())
                .unwrap();

            stream.flush().unwrap();
        }
    }

    fn parse_request(buffer: &[u8], web_root: &str) -> Option<Request> {
        let index_default = "/index.html";

        match RequestMethod::from(buffer) {
            RequestMethod::GET => {
                let data = String::from_utf8_lossy(&buffer[..]);
                let parts = data.splitn(3, ' ').take(2).collect::<Vec<_>>();

                if !parts.is_empty() && parts.len() > 1 {
                    let path = Path::new(parts[1]).to_str();

                    // TODO the house of cards below
                    Some(Request {
                        method: RequestMethod::GET,
                        path: match path {
                            Some(EVENT_PATH) => String::from(EVENT_PATH),
                            Some("/") => format!("{}{}", web_root, index_default),
                            _ => format!(
                                "{}{}",
                                web_root,
                                path.map(|s| s
                                    .splitn(2, "?")
                                    .take(1)
                                    .collect::<Vec<_>>()
                                    .first()
                                    .unwrap()
                                    .to_owned())
                                    .unwrap_or_else(|| "/")
                            ),
                        },
                    })
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_get_request() {
        let request = HttpHandler::parse_request("GET / HTTP1.1".as_bytes(), "");

        assert_eq!(request.unwrap().method, RequestMethod::GET)
    }

    #[test]
    fn parse_post_request() {
        let request = HttpHandler::parse_request("POST /test HTTP/1.1".as_bytes(), "");

        assert!(request.is_none());
    }

    #[test]
    fn parse_request_index_path() {
        let request = HttpHandler::parse_request("GET / HTTP1.1".as_bytes(), "/var/www/coolsite");

        assert_eq!(request.unwrap().path, "/var/www/coolsite/index.html")
    }

    #[test]
    fn parse_request_event_path() {
        let request = HttpHandler::parse_request("GET /events HTTP1.1".as_bytes(), "");

        assert_eq!(request.unwrap().path, EVENT_PATH)
    }

    #[test]
    fn parse_request_path() {
        let request = HttpHandler::parse_request(
            "GET /path/file.png?arg=value HTTP1.1".as_bytes(),
            "/var/www",
        );

        assert_eq!(request.unwrap().path, "/var/www/path/file.png")
    }
}

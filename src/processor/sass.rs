use super::{Processor, Update};
use std::path::PathBuf;

pub struct SassProcessor;

impl Processor for SassProcessor {
    fn process(update: Update, web_root: PathBuf) -> Option<Update> {
        println!("SASS_PROCESSOR RCV:\t\t{:?}", update.path);

        // Append the modified file path to the web_root
        let mut output_path = std::path::PathBuf::from(web_root);

        // Another .scss file may heva been updated, but we just recompile the 
        // main 'style.scss' and assume it's an import of that file
        output_path.push("style.scss");

        match grass::from_path(&output_path.to_str().unwrap(), &grass::Options::default()) {
            Ok(result) => {
                output_path.pop();
                output_path.push("./build/style.css");

                if let Err(error) = std::fs::write(&output_path, result) {
                    println!(
                        "Failed to write updated asset to {:?}: {:?}",
                        output_path, error
                    );
                }
            }
            Err(error) => {
                println!(
                    "Error while processing asset at {:?}: {:?}",
                    output_path, *error
                );
            }
        }

        None
    }
}

pub mod sass;

use super::filewatcher::Update;

pub trait Processor {
    fn process(update: Update, web_root: std::path::PathBuf) -> Option<Update>;
}

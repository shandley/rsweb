

const eventSource = new EventSource('//localhost:7878/events')

eventSource.onerror = e => {
    console.error('eventSource error', e)
}

eventSource.addEventListener("hello", () => console.info("EventStream connected!"))

eventSource.addEventListener("css", (data) => {
    let url = new URL(document.getElementById('stylesheet').href)
    url.hash = + new Date()

    document.getElementById('stylesheet').href = url.toString().replace(url.protocol, '')
})

eventSource.addEventListener("js", (message) => {
    console.log('javascript update', message.data)

    import(`/${message.data}?time=${+ new Date}`).then((component) => {
        console.log('reloaded component', component)

        component.reload()
    })
})

const id = "intro";
const parentId = "app";

export const create = () => {
    const header = document.createElement('h1');

    header.id = id;
    header.textContent = 'Hello, hot reloading';

    document.getElementById(parentId).appendChild(header);
};

export const remove = () => {
    if (document.getElementById(id)) {
        document.getElementById(parentId).removeChild(document.getElementById(id));
    }
}

export const reload = () => {
    remove()
    create()
}
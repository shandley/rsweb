const id = "app";

export const create = () => {
    const app = document.createElement('div');
    app.id = id;

    document.appendChild(app);
};

export const reload = () => {
    if (!document.getElementById(id)) {
        create()
    }
}

export const load = () => {
    create()
}
const id = "subtitle";
const parentId = "app";

export const create = () => {
    const header = document.createElement('h3');

    header.id = id;
    header.textContent = 'I need to sort out the ordering';

    document.getElementById(parentId).appendChild(header);
};

export const remove = () => {
    if (document.getElementById(id)) {

        document.getElementById(parentId).removeChild(document.getElementById(id));
    }
}

export const reload = () => {
    remove()
    create()
}
import { create, remove } from './createText.mjs';

const registry = {
    accept(dependencies, callback) {
        // console.log('registry::accept', dependencies, callback)

        // import(`./createText.mjs?time=${+ new Date}`).then((module) => {
        //     module.create();
        // })
    },

    dispose(originalModuleUrl) {
        // console.log('registry::dispose', originalModuleUrl)

        // import(`./createText.mjs?time=${+ new Date}`).then((module) => {
        //     module.remove();
        // })
    },

}

const hot = (moduleUrl) => {
    console.log('moduleUrl', moduleUrl);

    const originalModuleUrl = getOriginalUrl(moduleUrl);

    return {
        accept(dependecies, callback) {
            for (const dependecy of dependecies) {
                registry.accept(new URL(dependecy, originalModuleUrl).href, callback);
            }
            return this;
        },

        dispose(callback) {
            registry.dispose(originalModuleUrl, callback);
            return this;
        },

        selfAccept(callback) {
            registry.accept(originalModuleUrl, callback);
            return this;
        }
    };
}

const getOriginalUrl = (url) => {
    const urlObject = new URL(url);
    urlObject.searchParams.delete('mtime');

    return urlObject.href;
}

hot(import.meta.url)
    .dispose(() => remove())
    .selfAccept(() => create());
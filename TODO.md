### TODO

- Figure out the connection dropping bts of the websocket server, and clean up after connection is dropped
- redirect requests to `/build` and process thigs:
    - index.html should be augmented to point to hot reloading / processed things
    - Replace assets with version params, yabby-id values for consistent lookup, etc
- Handle Sass
- Handle Typescript? Assemblyscript? Something good
- Handle .babel style transpilation


const eventSource = new EventSource('//localhost:7878/events')

let count = 0;

eventSource.addEventListener("js", async (message) => {
    count++

    console.log('message', message)

    const { default: init, add } = await import('/wasm/pkg/wasm.js')

    let wasm = await init()

    console.log('wasm.add(1, 2)', wasm.add(1, 2))
    document.getElementById('event_count').innerHTML = count
})

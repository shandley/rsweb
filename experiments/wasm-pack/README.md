# Hot reloaded wasm

(without a bundler, or node)

Works by detecting `.js` files and re-importing the wasm, while the rust wasm code is rebuilt on change via `cargo watch`

![Image of example](./example.gif)

## Setup

Install `wasm-pack` via [thier website](https://rustwasm.github.io/wasm-pack/installer/)

`cargo install cargo-watch` to auto-rebuild changes to the rust code. Then from within the `wasm` path, auto-rebuild rust code via:

`cargo watch -i .gitignore -i "pkg/*" -s "wasm-pack build --target web"`

## References

https://rustwasm.github.io/docs/wasm-bindgen/examples/without-a-bundler.html

https://lannonbr.com/blog/2020-02-17-wasm-pack-webpack-plugin

https://github.com/wasm-tool/wasm-pack-plugin/blob/master/plugin.js
## CSS reloading example

Run me via:

`cargo run -- --path ./examples/css`

Then update `./style.css` and hit save to see updates in the browser. You may need to hit save a few times initially.
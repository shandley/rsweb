const parentId = "app";

export const id = "cool-component";

export const update = () => {
    const component = document.createElement('h1');

    component.id = id;
    component.textContent = 'Hello, edit this text and hit save (a few times)';

    if (document.getElementById(id)) {
        document.getElementById(id).replaceWith(component);
    } else {
        document.getElementById(parentId).appendChild(component);
    }
};

update()
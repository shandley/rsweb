## Javascript reloading example

Run me via:

`cargo run -- --path ./examples/components`

Then update `./components.mjs` and hit save to see updates in the browser. You may need to hit save a few times initially.
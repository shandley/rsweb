const eventSource = new EventSource('//localhost:7878/events')

eventSource.addEventListener("js", message => {
    // Re-import the module, calling the destructor/constructor on it
    // By appending the time parameter, the browser sees this as a new import
    import(`/${message.data}?time=${+ new Date}`).then((component) => {
        try {
            component.update()
        } catch {
            // ...
        }
    })
})

const eventSource = new EventSource('//localhost:7878/events')

eventSource.addEventListener("css", (message) => {
    document.getElementById('stylesheet').href = `${message.data}?time=${+ new Date}`
})

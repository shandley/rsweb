## SASS reloading example

This will build SASS files and write the output to `./build/style.css` before dispatching a CSS update event. 

Run me via:

`cargo run -- --path ./examples/sass`

Then update `./*.sass` and hit save to see updates in the browser. You may need to hit save a few times initially.
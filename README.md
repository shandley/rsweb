# htrldndvsrvr

(hot reloading dev server)

This is an toy HTTP server for experimenting with module reloading features of javascript. It runs a file watcher which will post messages in the form of [Server-sent events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events), which can be subscribed to in the browser, to be notified of file changes.

Once running you can add some javascript to a client application to get updates from the event endpoint:

```javascript
const eventSource = new EventSource('//localhost:7878/events')

eventSource.addEventListener("js", (message) => {
    // message with the URL of the javascript resource which has updated
})
```

It differs a little from usual implementations I've come across in that:

- It uses a uni-directional Server-sent event source to get update information (no websockets)
- It does not involve node/npm

It's a dubious implementation of a http server which is very limited, but it's hard to find implementations of Server-sent events which aren't part of a giant web framework in rust.

### Running

The server requires the rust [toolchain to be installed](https://rustup.rs/). Once installed, you can build and run with:

`cargo run`

By default, it will watch the directory that it starts in, but you can pass a path to watch/serve files from. 

To view some example usages, run with `cargo run -- --path ./experiments/{name}` where the name is one of:

| Name         | Notes                                                              |
|--------------|--------------------------------------------------------------------|
| `components` | Basic JS component reloads                                         |
| `sass`       | Uses `grass` for building .scss to .css and updating               |
| `wasm-pack`  | uses `wasm-pack` to rebuild and reload a simple webassembly module |

### The EventSource

The filewatcher currently reports messages based on file extension. So while the example above will catch `js` (which also include `.mjs` files) CSS changes can also be listened for:

```javascript
const eventSource = new EventSource('//localhost:7878/events')

eventSource.addEventListener("css", (message) => {
    // message with the URL of the css resource which has updated
})
```

### Known issues

- Sometimes you will need to save a file > 1 time after the eventstreams initial connection before updates flow in. I'm not sure why yet
- If you connect with > 1 client to the event stream, things break 
- File modification events don't evaluate whether the file data has changed, so they fire even if you save without changes. This has been greatly beneficial during testing, but I should probably fix that
- I need to get better at Rust

### Things I want to add

- Various things in the HTTP server to better handle files and paths
- HTTPS to enable service workers
- Pipelines which can catch file modification events and transform files before notifying the browser (or during ideally, it's pretty fast)

### Things I read and pondered:

https://github.com/kettle11/devserver

https://github.com/pikapkg/esm-hmr

https://github.com/progrium/hotweb

https://itnext.io/hot-reloading-native-es2015-modules-dc54cd8cca01

https://www.snowpack.dev/

https://github.com/SevInf/heiss

https://whatwg.github.io/loader/

https://doc.rust-lang.org/stable/book/ch20-01-single-threaded.html

### License

Code in this repository is primarily distributed under the terms of both the MIT license and the Apache License (Version 2.0).

See LICENSE-APACHE and LICENSE-MIT for details.